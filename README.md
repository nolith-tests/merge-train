# Merge Train

![Multi track drifting](multi_track_drifting.png)

Merge Train is a project used to automatically merge changes from a source
repository into a target repository. Changes can be merged using `git merge`, or
by copying over all changes except for proprietary files such as the `ee/`
directory in GitLab. In both cases merge conflicts are handled automatically.

## Strategies

There are two merge strategies provided by the Merge Train: a regular merge, and
a FOSS merge.

### Regular merge

A regular merge is the default approach, and uses `git merge` to merge changes
from the source repository into the target repository. Conflicts are resolved by
taking the version of the target repository.

### FOSS merge

A FOSS merge can be used to merge changes from a GitLab repository into a
repository that does not contain any proprietary code. This approach does not
use `git merge`, instead it applies all changes from the source repository
followed by removing any proprietary files and folders. To use this strategy,
pass the `foss` argument to the `merge-train` executable.

## Requirements

* Git
* Bash
* Push access to the target repository.
* Make (during development)

## Installation

Clone the repository:

    git@gitlab.com:gitlab-org/merge-train.git

Then you can run the merge train as follows to merge all CE `master` changes
into EE `master`:

    ./bin/merge-train

To merge changes from GitLab into a FOSS only repository, run the following
instead:

    SOURCE_PROJECT='gitlab-org/gitlab' TARGET_PROJECT='gitlab-org/gitlab-foss' ./bin/merge-train foss

This would merge all changes from gitlab-org/gitlab into gitlab-org/gitlab-foss,
and remove any proprietary code from the target repository.

## Using in CI

Setting up Merge Train in CI is pretty straightforward, simply add the following
to your `.gitlab-ci.yml`:

```yaml
stages:
  # ... other stages ...
  - merge

merge:master:
  image: registry.gitlab.com/gitlab-org/merge-train
  stage: merge
  only:
    refs:
      - schedules
    variables:
      - $MERGE_TRAIN_SSH_PUBLIC_KEY
      - $MERGE_TRAIN_SSH_PRIVATE_KEY
  script:
    - bash /app/bin/merge-train
  cache:
    paths:
      - gitlab-ce
      - gitlab-ee
    key: 'merge:master'
```

The script `merge-train` automatically clones (and caches) GitLab CE and EE
(configurable), merges changes from CE into EE using a time range, then pushes
the changes into EE.

The `merge-train` script requires the following variables to be set in CI:

1. `MERGE_TRAIN_SSH_PUBLIC_KEY`
1. `MERGE_TRAIN_SSH_PRIVATE_KEY`

To change the source and target repositories, change the `SOURCE_PROJECT` and
`TARGET_PROJECT` variables, respectively.

In addition to the above, it is possible to specify source and target branches,
by supplying `SOURCE_BRANCH` and `TARGET_BRANCH` variables.

## Updating known hosts

The known GitLab.com hosts are baked into the Docker image. To update this list,
run the following:

    make config/known_hosts

You can then commit the changes as usual.
